import ast = require('./impl/ast')
import gen = require('./impl/gen')
import qooxdooGenerate = require('./impl/qooxdoo_ts')

/**
 * Create/get module
 */
export function mod(name: string): ast.Module {
  return new ast.ModuleImpl(name)
}

/**
 * Create custom control
 */
export function custom(name: string, className: string, props?: any): ast.Control {
  return new ast.ControlImpl(name, gen.ControlType.CUSTOM, className, props);
}

/**
 * Create control
 */
function createControl(name: string, cType: gen.ControlType, props?: any): ast.Control {
  props = props || {}
  if(cType == gen.ControlType.CUSTOM)
    throw new Error('Use "custom()" function to create controls of that type.')
  return new ast.ControlImpl(name, cType, null, props)
}

export function label(name: string, label: string, props?: any) {
  props = props || {}
  props.alignY = props.alignY || 'middle'
  props.label = label
  return createControl(name, gen.ControlType.LABEL, props)
}

export function text(name: string, props?: any) {
  return createControl(name, gen.ControlType.TEXT, props)
}

export function pass(name: string, props?: any) {
  return createControl(name, gen.ControlType.PASSWORD, props)
}

export function check(name: string, label: string, props?: any) {
  props = props || {}
  props.label = label
  return createControl(name, gen.ControlType.CHECKBOX, props)
}

export function radio(name: string, label: string, props?: any) {
  props = props || {}
  props.label = label
  return createControl(name, gen.ControlType.RADIO, props)
}

export function radioGroup(name: string, props?: any) {
  return createControl(name, gen.ControlType.RADIOGROUP, props)
}

export function button(name: string, label: string, icon?: string, props?: any) {
  props = props || {}
  props.label = label
  props.icon = icon
  return createControl(name, gen.ControlType.BUTTON, props)
}

export function groupBox(name: string, label?: string, props?: any) {
  props = props || {}
  props.label = label
  return createControl(name, gen.ControlType.GROUPBOX, props)
}

export function date(name: string, props?: any) {
  return createControl(name, gen.ControlType.DATE, props)
}

export function integer(name: string, props?: any) {
  return createControl(name, gen.ControlType.INTEGER, props)
}

export function ipV4(name: string, props?: any) {
  return createControl(name, gen.ControlType.IPV4, props)
}

export function float(name: string, props?: any) {
  return createControl(name, gen.ControlType.FLOAT, props)
}

export function fraction(name: string, props?: any) {
  return createControl(name, gen.ControlType.FRACTION, props)
}

export function moneySum(name: string, props?: any) {
  return createControl(name, gen.ControlType.MONEYSUM, props)
}

export function time(name: string, props?: any) {
  return createControl(name, gen.ControlType.TIME, props)
}

export function panel(name: string, props?: any) {
  return createControl(name, gen.ControlType.PANEL, props)
}

export function combo(name: string, props?: any) {
  return createControl(name, gen.ControlType.COMBOBOX, props)
}

export function ref(name: string, props?: any) {
  return createControl(name, gen.ControlType.REFERENCE, props)
}

export function separator(name: string, label?: string,props?: any) {
  props = props || {}
  props.label = label
  return createControl(name, gen.ControlType.SEPARATOR, props)
}

export function tree(name: string, props?: any) {
  return createControl(name, gen.ControlType.TREE, props)
}

export function checkTree(name: string, props?: any) {
  return createControl(name, gen.ControlType.CHECKBOXTREE, props)
}

export function textArea(name: string, props?: any) {
  return createControl(name, gen.ControlType.TEXTAREA, props)
}

export function tabView(name: string, props?: any) {
  return createControl(name, gen.ControlType.TABVIEW, props)
}

export function table(name: string, scrollable: boolean, props?: any) {
  props = props || {}
  props.scrollable = scrollable
  return createControl(name, gen.ControlType.TABLE, props)
}

export function spin(name: string, min: number, max: number, value?: number, props?: any) {
  props = props || {}
  props.min = min
  props.max = max
  props.value = value
  return createControl(name, gen.ControlType.SPINNER, props)
}

export function cardView(name: string, props?: any) {
  return createControl(name, gen.ControlType.CARDVIEW, props)
}

export function scrollPane(name: string, props?: any) {
  return createControl(name, gen.ControlType.SCROLLPANE, props)
}

export function genQooxdooTsModule(m, p): string {
  var s = ''
  qooxdooGenerate(m, p ? p : gen.DEFAULT_SOURCE_PARAMS, d => { s += d })

  return s
}

