import ast = require('./ast')
import gen = require('./gen')

enum ReadOnlyType {
  READ_ONLY = 1,
  ENABLED
}

interface ControlSpec {
  ctorParams?: (p?: any) => string
  propsFlt?: string[]
  readOnly?: ReadOnlyType
  bindProp?: string
}

function lbl(name) {
  return name !== null && name !== undefined
    ? "'" + name + "'"
    : "null"
}

var CONTROL_SPECS: {[name: string]: ControlSpec} = {
  'LABEL': {
    ctorParams: p => { return lbl(p.label) },
    propsFlt: ['label']
  },
  'TEXT': {
    readOnly: ReadOnlyType.READ_ONLY,
    bindProp: 'value'
  },
  'PASSWORD': {
    readOnly: ReadOnlyType.READ_ONLY,
    bindProp: 'value'
  },
  'CHECKBOX': {
    ctorParams: p => { return lbl(p.label) },
    propsFlt: ['label'],
    readOnly: ReadOnlyType.ENABLED,
    bindProp: 'value'
  },
  'RADIOGROUP': {
    bindProp: 'rbValue'
  },
  'RADIO': {
    ctorParams: p => { return lbl(p.label) },
    propsFlt: ['label'],
    readOnly: ReadOnlyType.ENABLED,
    bindProp: 'value'
  },
  'BUTTON': {
    ctorParams: p => { return lbl(p.label) + (p.icon ? ', \'' + p.icon + '\'' : '') },
    propsFlt: ['label', 'icon'],
    readOnly: ReadOnlyType.ENABLED
  },
  'GROUPBOX': {
    ctorParams: p => { return lbl(p.label) },
    propsFlt: ['label']
  },
  'DATE': {
    readOnly: ReadOnlyType.READ_ONLY,
    bindProp: 'value'
  },
  'INTEGER': {
    readOnly: ReadOnlyType.READ_ONLY,
    bindProp: 'intValue'
  },
  'IPV4': {
    readOnly: ReadOnlyType.READ_ONLY,
    bindProp: 'value'
  },
  'FLOAT': {
    readOnly: ReadOnlyType.READ_ONLY,
    bindProp: 'floatValue'
  },
  'FRACTION': {
    readOnly: ReadOnlyType.READ_ONLY,
    bindProp: 'fractionValue'
  },
  'MONEYSUM': {
    readOnly: ReadOnlyType.READ_ONLY,
    bindProp: 'moneyValue'
  },
  'TIME': {
    readOnly: ReadOnlyType.READ_ONLY,
    bindProp: 'value'
  },
  'PANEL': {
    ctorParams: _ => { return 'null' }
  },
  'COMBOBOX': {
    readOnly: ReadOnlyType.ENABLED,
    bindProp: 'value'
  },
  'REFERENCE': {
    readOnly: ReadOnlyType.ENABLED,
    bindProp: 'value'
  },
  'SEPARATOR': {
    ctorParams: p => { return lbl(p.label) },
    propsFlt: ['label']
  },
  'TREE': {
    readOnly: ReadOnlyType.ENABLED,
    bindProp: 'selectedValue'
  },
  'CHECKBOXTREE': {
    readOnly: ReadOnlyType.ENABLED,
    bindProp: 'checkedValues'
  },
  'TEXTAREA': {
    readOnly: ReadOnlyType.READ_ONLY,
    bindProp: 'value'
  },
  'TABLE': {
    propsFlt: ['scrollable']
  },
  'SPINNER': {
    ctorParams: p => { return '' + (p.min || 0) + ', ' + (p.value || 0) + ', ' + (p.max || 0) },
    propsFlt: ['min', 'max', 'value'],
    readOnly: ReadOnlyType.ENABLED
  },
  'CUSTOM': {
    readOnly: ReadOnlyType.READ_ONLY
  }
}

interface OutFunc {
  (s: string): void
}

class GeneratorBase {
  constructor(public p: gen.SourceParams, public lvl: number, public out: OutFunc) {}

  tab(): string {
    var t = ''
    for(var i = 0; i < this.lvl*this.p.tabWidth; i++)
      t += ' '
    return t
  }

  line(s: string = '') {
    var data = ''
    if(!s) {
      data += '\n'
    } else {
      data += this.tab()
      data += s + '\n'
    }
    this.out(data)
  }

  jsValue(v: any): string {
    return JSON.stringify(v)
  }

  getterName(prop: string) {
    var res = ''
    prop.split('.').forEach(v => {
      if(res !== '')
        res += '.'
      res += 'get' + v.charAt(0).toUpperCase() + v.substring(1) + '()'
    })
    return res
  }
}

class LayoutGenerator extends GeneratorBase {

  constructor(private l: ast.LayoutImpl, private idx: number, p: gen.SourceParams, lvl: number, out: OutFunc) {
    super(p, lvl, out);
  }

  generateLayout() {
    this.line('var ' + this.varName() + ' = new qx.ui.layout.Grid(' + this.p.gridHGap + ', ' + this.p.gridVGap + ')');
    this.callSetLayout()
    var cols = this.l.cols || []
    var rows = this.l.rows || []
    cols.forEach(this.generateColSpec, this)
    rows.forEach(this.generateRowSpec, this)

    this.l.items.forEach(this.generateItemSpec, this)
  }

  private generateColSpec(c: ast.ColumnProps, idx: number) {
    if(!c) return
    if(c.flex)
      this.line(this.varName() + '.setColumnFlex(' + idx + ', ' + c.flex + ')')
    if(c.width)
      this.line(this.varName() + '.setColumnWidth(' + idx + ', ' + c.width + ')')
    if(c.alignX || c.alignY)
      this.line(this.varName() + '.setColumnAlign(' + idx + ', ' + this.jsValue(c.alignX || 'right') + ', ' + this.jsValue(c.alignY || 'middle') + ')')
  }

  private generateRowSpec(r: ast.RowProps, idx: number) {
    if(!r) return
    if(r.flex)
      this.line(this.varName() + '.setRowFlex(' + idx + ', ' + r.flex + ')')
    if(r.height)
      this.line(this.varName() + '.setRowHeight(' + idx + ', ' + r.height + ')')
    if(r.alignX || r.alignY)
      this.line(this.varName() + '.setRowAlign(' + idx + ', ' + this.jsValue(r.alignX || 'right') + ', ' + this.jsValue(r.alignY || 'middle') + ')')
  }

  private generateItemSpec(i: ast.LayoutItem) {
    this.callAddItem(i)

  }

  private callSetLayout() {
    if(this.l.ctrl === this.l.cls) {
      this.line('this._setLayout(' + this.varName() + ')')
    } else {
      this.line('this.' + (<ast.ControlImpl>this.l.ctrl).name + '.setLayout(' + this.varName() + ')')
    }
  }

  private callAddItem(i: ast.LayoutItem) {
    if(this.l.ctrl === this.l.cls) {
      this.line('this._add(this.' + i.c.name + ', { row: ' + i.y + ', column: ' + i.x + ', rowSpan: ' + i.rows + ', colSpan: ' + i.cols + '})')
    } else {
      this.line('this.' + (<ast.ControlImpl>this.l.ctrl).name + '.add(this.' + i.c.name + ', { row: ' + i.y + ', column: ' + i.x + ', rowSpan: ' + i.rows + ', colSpan: ' + i.cols + '})')
    }
  }

  private varName() {
    return 'layout' + (this.idx === 0 ? '' : '' + this.idx)
  }
}

class ClassGenerator extends GeneratorBase {

  private tabPageIdx = 0

  constructor(private cls: ast.ClassImpl, p: gen.SourceParams, lvl: number, out: OutFunc) {
    super(p, lvl, out)
  }

  generateClass() {
    this.line('export class ' + this.cls.name + ' extends qx.ui.core.Widget implements tslib.ui.ReadOnly, tslib.ui.Validatable {')
    this.line()
    this.lvl++

    var hasBind = this.cls.hasBind()

    if(hasBind) {
      this.line('bindCtrl: qx.data.controller.Object = null')
      this.line('bindModel: any = null')
      this.line()
    }

    for(var ctrl in this.cls.controls) {
      var c = this.cls.controls[ctrl]
      // TODO: Fix control class names
      var className = gen.getClassName(gen.GeneratorType.QOOXDOO_TS, c)
      if(className.match(/^eldisjs\./))
        className = 'any'
      this.line(c.name + ': ' + className)
    }

    this.line()


    this.line('constructor() {')
    this.lvl++
    this.line('super()')
    this.line('this.initControls()')
    if(this.cls.l)
      this.line('this.setupLayout()')
    this.lvl--
    this.line('}')
    this.line()

    this.generateInitControls()
    if(this.cls.l)
      this.generateLayout()
    this.generateReadOnly()

    if(hasBind) {
      this.generateSetupModel()
      this.generateInitBindings()
      this.generateGetModel()
      this.generateSetModel()
      this.genetateCleanup()
    }

    this.generateInitUiBindings()

    this.generateValidate()

    this.lvl--
    this.line('}')
  }

  private generateInitControls() {
    this.line('private initControls() {')
    this.lvl++

    Object.keys(this.cls.controls).map(n => { return this.cls.controls[n] }).forEach(c => {
      var spec = CONTROL_SPECS[gen.ControlType[c.cType]] || {
        ctorParams: () => { return '' }
      }
      this.generateCreateControl(c, spec)
    })

    Object.keys(this.cls.controls).map(n => { return this.cls.controls[n] }).filter(c => { return c.childs.length !== 0 }).forEach(c => {
      this.generateAddChilds(c)
    })

    this.lvl--
    this.line('}')
    this.line()
  }

  private generateReadOnly() {
    this.line('private __gen_isReadOnly = false')
    this.line()
    this.line('isReadOnly(): boolean {')
    this.lvl++
    this.line('return this.__gen_isReadOnly')
    this.lvl--
    this.line('}')
    this.line()

    this.line('setReadOnly(readOnly: boolean) {')
    this.lvl++

    this.line('this.__gen_isReadOnly = readOnly;')
    Object.keys(this.cls.controls).map(n => this.cls.controls[n]).forEach(c => {
      var spec = CONTROL_SPECS[gen.ControlType[c.cType]]
      if(!spec || !spec.readOnly)
        return
      switch(spec.readOnly) {
        case ReadOnlyType.READ_ONLY:
          this.line('this.' + c.name + '.setReadOnly(readOnly);')
          break
        case ReadOnlyType.ENABLED:
          this.line('this.' + c.name + '.setEnabled(!readOnly);')
          break
      }
    })

    this.lvl--
    this.line('}')
    this.line()
  }

  generateAddChilds(c: ast.ControlImpl) {
    c.childs.forEach(ch => {
      if(c.cType === gen.ControlType.TABVIEW) {
        this.tabPageIdx++
        this.line('var tabPage' + this.tabPageIdx + ' = new qx.ui.tabview.Page(' + this.jsValue(ch.props['tabLabel']) + ', null)')
        this.line('tabPage' + this.tabPageIdx + '.setLayout(new qx.ui.layout.Canvas())')
        this.line('tabPage' + this.tabPageIdx + '.add(this.' + ch.name + ', { edge: 0 })')
        this.line('this.' + c.name + '.add(tabPage' + this.tabPageIdx + ')')
      } else if(c.cType === gen.ControlType.CARDVIEW) {
        this.line('this.' + c.name + '.addItem(this.' + ch.name + ', ' + this.jsValue(ch.props['cardName']) + ')')
      } else if(c.cType === gen.ControlType.RADIOGROUP) {
        this.line('this.' + c.name + '.addItem(this.' + ch.name + ', ' + this.jsValue(ch.props['rbValue']) + ')')
      } else
        this.line('this.' + c.name + '.add(this.' + ch.name + ')')
    })
  }

  private generateCreateControl(c: ast.ControlImpl, s: ControlSpec) {
    var l = 'this.' + c.name + ' = new ' + gen.getClassName(gen.GeneratorType.QOOXDOO_TS, c) + '(' + (s.ctorParams ? s.ctorParams(c.props) : '') + ')'
    var p = {}
    if(c.props) {
      if((typeof c.props) !== 'object')
        throw new Error('Control ' + c.name + ' contains bad props of type ' + (typeof c.props))
      for(var pn in c.props || {}) {
        if((s.propsFlt && s.propsFlt.indexOf(pn) !== -1)
          || pn === 'tabLabel'
          || pn === 'cardName'
          || pn === 'rbValue')
          continue
        p[pn] = c.props[pn]
      }
    }
    this.line(l + (Object.keys(p).length !== 0 ? '.set({' : ''))
    if(Object.keys(p).length !== 0) {
      this.lvl++
      var pc = Object.keys(p).length
      for(pn in p) {
        pc--
        this.line(pn + ': ' + this.jsValue(p[pn]) + (pc == 0 ? '' : ','))
      }
      this.lvl--
      this.line('})')
    }
  }

  private generateSetupModel() {
    this.line('setupModel(m: any): any {')
    this.lvl++

    this.line('if(!m) throw new Error(\'' + this.cls.name + '.setupModel called with undefined model\')')
    this.line()

    var generatedObjects: string[] = []

    this.cls.getBindControls()
      .forEach(v => {
        this.generateControlModel(v.bindProp, v.cType === gen.ControlType.CHECKBOXTREE ? '[]' : 'null', generatedObjects, v)
      })

    this.cls.bindProps.forEach(v => this.generateControlModel(v, 'null', generatedObjects))

    this.line()
    this.line('return m')

    this.lvl--
    this.line('}')
    this.line()
  }

  private generateControlModel(name: string, initializer: string, generated: string[], c?: ast.ControlImpl) {
    if(c && c.cType === gen.ControlType.CUSTOM && !name) {
      // Inline panel
      this.line('this.' + c.name + '.setupModel(m)')
      return
    }

    var genInitializer = (path: string, initializer: string): string => {
      return `(m.${path} === null || m.${path} === undefined) ? ${initializer} : m.${path}`
    }

    var path:string = null
    var spl = name.split('.')
    var curr = 0
    spl.forEach(n => {
      curr++
      path = (path === null ? '' : path + '.') + n
      if(generated.indexOf(path) >= 0)
        return
      if(curr === spl.length && c && c.cType === gen.ControlType.CUSTOM && name !== null) {
        // Child panel
        this.line(`m.${path} = this.${c.name}.setupModel(${genInitializer(path, '{}')})`)
      } else {
        this.line(`m.${path} = ${genInitializer(path, curr === spl.length ? initializer : '{}')}`)
      }
      generated.push(path)
    })
  }

  generateCreateBindModel() {
    this.line('createBindModel(jsonModel: any) {')
    this.lvl++

    this.line('this.bindModel = qx.data.marshal.Json.createModel(jsonModel, false)')

    this.lvl--
    this.line('}')
    this.line()
  }


  private generateInitBindings() {
    this.line('initBindings(qxModel: any) {')
    this.lvl++

    this.line('this.bindModel = qxModel')
    this.line('this.bindCtrl = new qx.data.controller.Object(qxModel)')
    this.line()

    this.cls.getBindControls()
      .forEach(v => {
        if(v.cType === gen.ControlType.CUSTOM) {
          this.line('this.' + v.name + '.initBindings(' + (v.bindProp ? 'this.bindModel.' + this.getterName(v.bindProp) : 'this.bindModel' ) + ')')
          return
        }
        var spec = CONTROL_SPECS[gen.ControlType[v.cType]]
        if(!spec || !spec.bindProp)
          return
        this.line('this.bindCtrl.addTarget(this.' + v.name + ', "' + spec.bindProp + '", "' + v.bindProp + '", true)')
      })

    this.lvl--
    this.line('}')
    this.line()
  }

  genetateCleanup() {
    this.line('cleanupModel(m: any) {')
    this.lvl++

    this.cls.getUiBind()
      .filter(c => c.bindProp !== null)
      .forEach(c => {
        var d = c.deps
        var spec = CONTROL_SPECS[gen.ControlType[d.c.cType]] || { bindProp: null }
        var value = c.deps.prop ? c.deps.prop : (spec.bindProp ? spec.bindProp : null)
        if(!value)
          return
        value = 'get' + value[0].toUpperCase() + value.slice(1)
        this.line('if(!this.' + c.deps.c.name + '.' + value + '()) delete m.' + c.bindProp)
      })

    this.line('return m')

    this.lvl--
    this.line('}')
    this.line()
  }

  generateGetModel() {
    this.line('getModel(parentModel?: any): any {')
    this.lvl++

    this.line('if(!this.bindModel) throw new Error(\'' + this.cls.name + '.getModel has no model\')')
    this.line()

    this.line('var m = <any>qx.util.Serializer.toNativeObject(this.bindModel, null, null)')
    this.cls.getBindControls()
      .filter(_ => _.cType === gen.ControlType.CUSTOM && !_.bindProp)
      .forEach(c => this.line('this.' + c.name + '.getModel(m)'))
    this.line()

    this.line('if(parentModel !== undefined) {')
    this.lvl++

    this.line('tslib.core.extendObject(parentModel, m, false, true)')
    this.line('m = parentModel')

    this.lvl--
    this.line('}')
    this.line()

    var c = this.cls.getBindControls()
      .filter(_ => _.cType === gen.ControlType.CUSTOM && !!_.bindProp)
    c.forEach(c => this.line('m.' + c.bindProp + ' = this.' + c.name + '.getModel()'))
    if(c.length !== 0)
      this.line()

    this.line('m = this.cleanupModel(m)')
    this.line()

    this.line('return m')

    this.lvl--
    this.line('}')
    this.line()
  }

  generateSetModel() {
    this.line('setModel(m: any): void {')
    this.lvl++

    this.line('var m = this.setupModel(m || {})')
    this.line('this.initBindings(qx.data.marshal.Json.createModel(m, false))')
    this.line('this.initUiBindings()')
    this.line()
    this.cls.getUiBind()
    .filter(c => c.bindProp !== null)
    .forEach(c => {
      var d = c.deps
      var spec = CONTROL_SPECS[gen.ControlType[d.c.cType]] || { bindProp: null }
      var value = c.deps.prop ? c.deps.prop : (spec.bindProp ? spec.bindProp : null)
      if(!value)
        return
      value = 'set' + value[0].toUpperCase() + value.slice(1)
      this.line('this.' + d.c.name + '.' + value + '(!(!m.' + c.bindProp + ' || (Array.isArray(m.' + c.bindProp + ') && m.' + c.bindProp + '.length === 0)))')
    })

    this.lvl--
    this.line('}')
    this.line()
  }

  generateInitUiBindings() {
    this.line('initUiBindings(): void {')
    this.lvl++

    this.cls.getUiBind()
      .forEach(c => {
        var d = c.deps
        var spec = CONTROL_SPECS[gen.ControlType[d.c.cType]] || { bindProp: null }
        var value = c.deps.prop ? c.deps.prop : (spec.bindProp ? spec.bindProp : null)
        if(!value)
          return
        this.line('this.' + d.c.name + '.bind("' + value + '", this.' + c.name + ', "' + (c.cType === gen.ControlType.CARDVIEW ? 'selectedItem' : 'enabled') + '", null)')
      })

    this.cls.getBindControls()
      .filter(_ => _.cType === gen.ControlType.CUSTOM)
      .forEach(_ => this.line('this.' + _.name + '.initUiBindings()'))

    this.lvl--
    this.line('}')
    this.line()
  }

  generateValidate() {
    this.line('validate(): tslib.core.Promise<boolean> {')
    this.lvl++

    var v = this.cls.validators
    if(v.length === 0) {
      this.line('return tslib.core.mkPromise(true)')
    } else {
      this.line('return tslib.ui.validator()')
      this.lvl++

      v.forEach(v => {
        switch(v.t) {
          case ast.ValidatorType.NOT_EMPTY:
            this.line('.notEmpty(this.' + v.v.name + ', ' + this.jsValue(v.msg) + ')')
            break
          case ast.ValidatorType.NOT_ZERO:
            this.line('.notZero(this.' + v.v.name + ', ' + this.jsValue(v.msg) + ')')
            break
          case ast.ValidatorType.NOT_EMPTY_INC:
            this.line('.notEmptyInc(this.' + v.inc.name + ', this.' + v.v.name + ', ' + this.jsValue(v.msg) + ')')
            break
          case ast.ValidatorType.NOT_ZERO_INC:
            this.line('.notZeroInc(this.' + v.inc.name + ', this.' + v.v.name + ', ' + this.jsValue(v.msg) + ')')
            break
          default:
            throw new Error('Unknown validator type ' + ast.ValidatorType[v.t])
        }
      })

      this.line('.validate()')
      this.lvl--
    }

    this.lvl--
    this.line('}')
    this.line()
  }

  generateLayout() {
    this.line('private setupLayout() {')
    this.lvl++

    new LayoutGenerator(this.cls.l, 0, this.p, this.lvl, this.out).generateLayout()

    var idx = 1

    Object.keys(this.cls.controls).map(n => { return this.cls.controls[n] }).filter(c => { return !!c.l }).forEach(c => {
      this.line()
      new LayoutGenerator(c.l, idx, this.p, this.lvl, this.out).generateLayout()
      idx++
    })

    this.lvl--
    this.line('}')
    this.line()
  }
}

class ModuleGenerator extends GeneratorBase {
  private data = ''

  constructor(private m: ast.ModuleImpl, p: gen.SourceParams, out: OutFunc) {
    super(p, 0, out)
  }

  generateModule() {
    this.lvl = 0
    this.generateImport()
    this.line('module ' + this.m.name + ' {')
    this.line()

    this.lvl++
    for(var c in this.m.classes) {
      this.generateClass(this.m.classes[c])
    }
    this.lvl--
    this.line('}')
  }

  private generateImport() {
    if(this.m.params && this.m.params.qxImport) {
      this.line('/// <reference path="' + this.m.params.qxImport + '" />')
      this.line()
    }
  }

  private generateClass(cls: ast.ClassImpl) {
    new ClassGenerator(cls, this.p, this.lvl, this.out).generateClass()
  }

}

function generate(o, p, out) {
  var g = new ModuleGenerator(o, p, out)
  g.generateModule()
}

export = generate
