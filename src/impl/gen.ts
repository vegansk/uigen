import ast = require('./ast')

/**
 * Generator's type
 */
export enum GeneratorType {
  QOOXDOO_TS
}

/**
 * Supported control types
 */
export enum ControlType {
  LABEL,
  TEXT,
  PASSWORD,
  CHECKBOX,
  RADIOGROUP,
  RADIO,
  BUTTON,
  GROUPBOX,
  DATE,
  INTEGER,
  IPV4,
  FLOAT,
  FRACTION,
  MONEYSUM,
  TIME,
  PANEL,
  COMBOBOX,
  REFERENCE,
  SEPARATOR,
  TREE,
  CHECKBOXTREE,
  TEXTAREA,
  TABVIEW,
  TABLE,
  SPINNER,
  CARDVIEW,
  SCROLLPANE,
  CUSTOM
}

export interface ControlClasses {
  [index: string]: any;
}
export interface GeneratorClasses {
  [index: string]: ControlClasses;
}

export var CONTROL_CLASSES: GeneratorClasses = {
  'QOOXDOO_TS': {
    'LABEL': 'qx.ui.basic.Label',
    'TEXT': 'tslib.ui.TextField',
    'PASSWORD': 'qx.ui.form.PasswordField',
    'CHECKBOX': 'qx.ui.form.CheckBox',
    'RADIOGROUP': 'tslib.ui.RadioGroup',
    'RADIO': 'qx.ui.form.RadioButton',
    'BUTTON': 'qx.ui.form.Button',
    'GROUPBOX': 'qx.ui.groupbox.GroupBox',
    'DATE': 'tslib.ui.DateField',
    'INTEGER': 'tslib.ui.IntegerField',
    'IPV4': 'tslib.ui.IpV4Field',
    'FLOAT': 'tslib.ui.FloatField',
    'FRACTION': 'tslib.ui.FractionField',
    'MONEYSUM': 'tslib.ui.MoneyField',
    'TIME': 'tslib.ui.TimeField',
    'PANEL': 'qx.ui.container.Composite',
    'COMBOBOX': 'qx.ui.form.ComboBox',
    'REFERENCE': 'tslib.ui.ReferenceBox',
    'SEPARATOR': 'tslib.ui.Separator',
    'TREE': 'tslib.ui.Tree',
    'CHECKBOXTREE': 'tslib.ui.CheckBoxTree',
    'TEXTAREA': 'tslib.ui.TextArea',
    'TABVIEW': 'qx.ui.tabview.TabView',
    'TABLE': (props: any) =>  {
                                if(props.scrollable)
                                  return 'tslib.ui.ScrollableTable'
                                else
                                  return 'tslib.ui.Table'
                              },
    'SPINNER': 'qx.ui.form.Spinner',
    'CARDVIEW': 'tslib.ui.CardPanel',
    'SCROLLPANE': 'qx.ui.container.Scroll'
  }
}

export function getClassName(t: GeneratorType, c: ast.ControlImpl): string {
  if(c.cType === ControlType.CUSTOM)
    return c.className

  var cn = CONTROL_CLASSES[GeneratorType[t]][ControlType[c.cType]]
  if(!cn)
    throw new Error('Cannot find class name for generator ' + GeneratorType[t] + ', control type ' + ControlType[c.cType])
  if(typeof cn === 'string')
    return cn

  return <string>cn(c.props)
}
/**
 * Source generation parameters
 */
export interface SourceParams {
  tabWidth: number
  gridHGap?: string
  gridVGap?: string

}

/**
 * Default source generation parameters
 */
export var DEFAULT_SOURCE_PARAMS: SourceParams = {
  tabWidth: 2,
  gridHGap: '5',
  gridVGap: '5'
}

