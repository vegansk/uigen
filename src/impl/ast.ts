import gen = require('./gen')

/**
 * Control properties, applied via rows and columns description too
 */
export interface ControlBaseProps {
  alignX?: string
  alignY?: string
  allowGrowX?: boolean
  allowGrowY?: boolean
}

/**
 * Control only properties
 */
export interface ControlProps {
  rowSpan?: number
  colSpan?: number
}

/**
 * Column properties
 */
export interface ColumnProps extends ControlBaseProps {
  flex?: number
  width?: number
}

/**
 * Row properties
 */
export interface RowProps extends ControlBaseProps {
  flex?: number
  height?: number
}

/**
 * Module interface
 */
export interface Module {
  setParams(p: any): Module
  cls(name: string): Class
}

/**
 * Layout interface
 */
export interface Layout {
  add(c: Control, p?: ControlProps): Layout
  addAll(c: Control[]): Layout
  skip(x?: number, y?: number): Layout
  to(x: number, y: number): Layout
  endl(): Layout
}

/**
 * Control interface
 */
export interface Control {
  layout(cols?: ColumnProps[], rows?: RowProps[]): Layout
  add(c: Control): Control
  bind(prop?: string): Control
  dep(c: Control, prop?: string): Control
}

/**
 * Class interface
 */
export interface Class extends Control {
  add(c: Control): Class
  get(name: string): Control

  notEmpty(v: Control, msg?: string): Class
  notZero(v: Control, msg?: string): Class
  notEmptyInc(inc: Control, v: Control, msg?: string): Class
  notZeroInc(inc: Control, v: Control, msg?: string): Class
}

export class ModuleImpl implements Module {
  classes: {[name: string]: ClassImpl} = {}
  params: any

  constructor(public name: string) {}

  setParams(p: any): Module {
    this.params = p
    return this
  }

  cls(name: string) {
    if(this.classes[name])
      throw new Error('Attempt to redefine class ' + name + ' in module ' + this.name)

    this.classes[name] = new ClassImpl(name, this)

    return this.classes[name]
  }
}

export enum ValidatorType {
  NOT_EMPTY = 1,
  NOT_ZERO,
  NOT_EMPTY_INC,
  NOT_ZERO_INC
}

export interface Validator {
  t: ValidatorType
  inc?: ControlImpl
  v: ControlImpl
  msg?: string
}

export class ClassImpl implements Class {
  controls: {[name: string]: ControlImpl} = {}
  l: LayoutImpl
  bindProps: string[] = []
  validators: Validator[] = []

  constructor(public name: string, public mod: ModuleImpl) {}

  add(c: Control) {
    var ctrl = <ControlImpl>c
    if(this.controls[ctrl.name] && this.controls[ctrl.name] !== ctrl)
      throw new Error('Attempt to redefine control ' + ctrl.name + ' in class ' + this.name + ', module ' + this.mod.name)

    this.controls[ctrl.name] = ctrl
    ctrl.cls = this

    return this
  }

  get(name: string): Control {
    return this.controls[name]
  }

  layout(cols?: ColumnProps[], rows?: RowProps[]) {
    if(this.l)
      throw new Error('Layout already set in class ' + this.name + ', module ' + this.mod.name)
    this.l = new LayoutImpl(this, this, cols, rows)
    return this.l
  }

  bind(prop?: string): Control {
    if(!prop)
      throw new Error('Cannot bind class ' + this.mod.name + '.' + this.name + ' to itself')
    if(this.bindProps.indexOf(prop) >= 0)
      return
    this.bindProps.push(prop)
    return this
  }

  dep(c: Control, prop?: string): Control {
    throw new Error('Class cannot be dependet on the other control')
  }

  hasBind(): boolean {
    return this.bindProps.length !== 0 || this.getBindControls().length !== 0
  }

  getBindControls(): ControlImpl[] {
    return Object.keys(this.controls)
      .map(k => this.controls[k])
      .filter(v => v.needToBind)
  }

  hasUiBind(): boolean {
      return this.getUiBind().length !== 0
  }

  getUiBind(): ControlImpl[] {
    return Object.keys(this.controls)
      .map(k => this.controls[k])
      .filter(v => v.deps !== null)
  }

  notEmpty(v: Control, msg?: string): Class {
    this.validators.push({ t: ValidatorType.NOT_EMPTY, v: <ControlImpl>v, msg: msg })
    return this
  }

  notZero(v: Control, msg?: string): Class {
    this.validators.push({ t: ValidatorType.NOT_ZERO, v: <ControlImpl>v, msg: msg })
    return this
  }

  notEmptyInc(inc: Control, v: Control, msg?: string): Class {
    this.validators.push({ t: ValidatorType.NOT_EMPTY_INC, inc: <ControlImpl>inc, v: <ControlImpl>v, msg: msg })
    return this
  }

  notZeroInc(inc: Control, v: Control, msg?: string): Class {
    this.validators.push({ t: ValidatorType.NOT_ZERO_INC, inc: <ControlImpl>inc, v: <ControlImpl>v, msg: msg })
    return this
  }
}

export interface LayoutItem {
  c: ControlImpl
  x: number
  y: number
  cols: number
  rows: number
}

export class LayoutImpl implements Layout {

  items: LayoutItem[] = []
  x = 0
  y = 0

  constructor(public cls: ClassImpl, public ctrl: Control, public cols?: ColumnProps[], public rows?: RowProps[]) {}

  add(c: Control, p?: ControlProps) {
    var i = {
      c: <ControlImpl>c,
      x: this.x,
      y: this.y,
      cols: p && p.colSpan ? p.colSpan : 1,
      rows: p && p.rowSpan ? p.rowSpan : 1
    }
    this.cls.add(i.c)
    this.items.push(i)

    this.x = this.x + i.cols

    return this
  }

  addAll(c: Control[]) {
    c.forEach(ctrl =>{
      this.add(ctrl)
    })
    return this
  }

  skip(x?: number, y?: number) {
    if(!x && !y)
      x = 1
    this.x = this.x + (x ? x : 0)
    this.y = this.y + (y ? y : 0)
    return this
  }

  to(x: number, y: number) {
    this.x = x
    this.y = y
    return this
  }

  endl() {
    this.x = 0
    this.y = this.y + 1
    return this
  }
}

export interface Dep {
  c: ControlImpl
  prop?: string
}

export class ControlImpl implements Control {

  cls: ClassImpl
  l: LayoutImpl
  childs: ControlImpl[] = []
  deps: Dep = null
  
  needToBind = false
  bindProp: string = null

  constructor(public name: string, public cType: gen.ControlType, public className?: string, public props?: any) {}

  layout(cols?: ColumnProps[], rows?: RowProps[]) {
    var cDesc = gen.ControlType[this.cType] + '[' + this.name + ']'
    if(!this.cls)
      throw new Error('Can\'t create layout for control ' + cDesc + ' without class')
    if(this.l)
      throw new Error('Attempt to redefine control ' + cDesc + ' layout')
    this.l = new LayoutImpl(this.cls, this, cols, rows)
    return this.l
  }

  add(c: Control) {
    var cimpl = <ControlImpl>c
    if(!this.cls) {
      throw new Error('Can\'t add control ' + gen.ControlType[cimpl.cType] + '[' + cimpl.name  +  '] to the parent ' + gen.ControlType[this.cType] + '[' + this.name + ']  without class');
    }

    this.childs.push(cimpl)
    this.cls.add(cimpl)
    return this
  }

  bind(prop?: string): Control {
    if(!prop && this.cType !== gen.ControlType.CUSTOM)
      throw new Error('Can\'t bind control ' + this.name + ' of type ' + gen.ControlType[this.cType] + ' to the parent\'s model')
    this.bindProp = prop || null
    this.needToBind = true
    return this
  }

  dep(c: Control, prop?: string): Control {
    this.deps = { c: <ControlImpl>c, prop: prop }
    return this
  }
}


