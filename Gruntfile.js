module.exports = function(grunt) {

  var SRC_ROOT='src/',
      BUILD_ROOT='build/';

  grunt.initConfig({
    ts: {
      default: {
        src: SRC_ROOT + '**/*.ts',
        outDir: BUILD_ROOT
      },
      options: {
        compiler: 'node_modules/typescript/bin/tsc.js',
        comments: false,
        module: 'commonjs',
        declaration: true
      }
    }
  });

  grunt.loadNpmTasks('grunt-ts');
  grunt.registerTask('default', ['ts']);
};
