var uigen = require('./uigen');
var path = require('path');
var fs = require('fs');
var args = process.argv.slice(2);
function usage() {
    console.log('Usage: uigen srcPath srcFile [dstFile]');
    process.exit(1);
}
if (args.length === 0 || args.length > 3)
    usage();
require('typescript-require')({
    dstRoot: path.join(__dirname, 'generated'),
    srcRoot: path.resolve(args[0])
});
var srcFile = path.resolve(args[1]);
var dstFile = args[2];
try {
    var m = require(srcFile);
    var dst = uigen.genQooxdooTsModule(m, null);
    if (dstFile)
        fs.writeFileSync(dstFile, dst);
    else
        console.log(dst);
}
catch (ex) {
    console.error('Error parsing ' + srcFile + ': ' + ex);
    process.exit(2);
}
//# sourceMappingURL=app.js.map