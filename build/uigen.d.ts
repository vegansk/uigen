import ast = require('./impl/ast');
export declare function mod(name: string): ast.Module;
export declare function custom(name: string, className: string, props?: any): ast.Control;
export declare function label(name: string, label: string, props?: any): ast.Control;
export declare function text(name: string, props?: any): ast.Control;
export declare function pass(name: string, props?: any): ast.Control;
export declare function check(name: string, label: string, props?: any): ast.Control;
export declare function radio(name: string, label: string, props?: any): ast.Control;
export declare function radioGroup(name: string, props?: any): ast.Control;
export declare function button(name: string, label: string, icon?: string, props?: any): ast.Control;
export declare function groupBox(name: string, label?: string, props?: any): ast.Control;
export declare function date(name: string, props?: any): ast.Control;
export declare function integer(name: string, props?: any): ast.Control;
export declare function ipV4(name: string, props?: any): ast.Control;
export declare function float(name: string, props?: any): ast.Control;
export declare function fraction(name: string, props?: any): ast.Control;
export declare function moneySum(name: string, props?: any): ast.Control;
export declare function time(name: string, props?: any): ast.Control;
export declare function panel(name: string, props?: any): ast.Control;
export declare function combo(name: string, props?: any): ast.Control;
export declare function ref(name: string, props?: any): ast.Control;
export declare function separator(name: string, label?: string, props?: any): ast.Control;
export declare function tree(name: string, props?: any): ast.Control;
export declare function checkTree(name: string, props?: any): ast.Control;
export declare function textArea(name: string, props?: any): ast.Control;
export declare function tabView(name: string, props?: any): ast.Control;
export declare function table(name: string, scrollable: boolean, props?: any): ast.Control;
export declare function spin(name: string, min: number, max: number, value?: number, props?: any): ast.Control;
export declare function cardView(name: string, props?: any): ast.Control;
export declare function scrollPane(name: string, props?: any): ast.Control;
export declare function genQooxdooTsModule(m: any, p: any): string;
