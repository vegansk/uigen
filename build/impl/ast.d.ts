import gen = require('./gen');
export interface ControlBaseProps {
    alignX?: string;
    alignY?: string;
    allowGrowX?: boolean;
    allowGrowY?: boolean;
}
export interface ControlProps {
    rowSpan?: number;
    colSpan?: number;
}
export interface ColumnProps extends ControlBaseProps {
    flex?: number;
    width?: number;
}
export interface RowProps extends ControlBaseProps {
    flex?: number;
    height?: number;
}
export interface Module {
    setParams(p: any): Module;
    cls(name: string): Class;
}
export interface Layout {
    add(c: Control, p?: ControlProps): Layout;
    addAll(c: Control[]): Layout;
    skip(x?: number, y?: number): Layout;
    to(x: number, y: number): Layout;
    endl(): Layout;
}
export interface Control {
    layout(cols?: ColumnProps[], rows?: RowProps[]): Layout;
    add(c: Control): Control;
    bind(prop?: string): Control;
    dep(c: Control, prop?: string): Control;
}
export interface Class extends Control {
    add(c: Control): Class;
    get(name: string): Control;
    notEmpty(v: Control, msg?: string): Class;
    notZero(v: Control, msg?: string): Class;
    notEmptyInc(inc: Control, v: Control, msg?: string): Class;
    notZeroInc(inc: Control, v: Control, msg?: string): Class;
}
export declare class ModuleImpl implements Module {
    name: string;
    classes: {
        [name: string]: ClassImpl;
    };
    params: any;
    constructor(name: string);
    setParams(p: any): Module;
    cls(name: string): ClassImpl;
}
export declare enum ValidatorType {
    NOT_EMPTY = 1,
    NOT_ZERO = 2,
    NOT_EMPTY_INC = 3,
    NOT_ZERO_INC = 4,
}
export interface Validator {
    t: ValidatorType;
    inc?: ControlImpl;
    v: ControlImpl;
    msg?: string;
}
export declare class ClassImpl implements Class {
    name: string;
    mod: ModuleImpl;
    controls: {
        [name: string]: ControlImpl;
    };
    l: LayoutImpl;
    bindProps: string[];
    validators: Validator[];
    constructor(name: string, mod: ModuleImpl);
    add(c: Control): ClassImpl;
    get(name: string): Control;
    layout(cols?: ColumnProps[], rows?: RowProps[]): LayoutImpl;
    bind(prop?: string): Control;
    dep(c: Control, prop?: string): Control;
    hasBind(): boolean;
    getBindControls(): ControlImpl[];
    hasUiBind(): boolean;
    getUiBind(): ControlImpl[];
    notEmpty(v: Control, msg?: string): Class;
    notZero(v: Control, msg?: string): Class;
    notEmptyInc(inc: Control, v: Control, msg?: string): Class;
    notZeroInc(inc: Control, v: Control, msg?: string): Class;
}
export interface LayoutItem {
    c: ControlImpl;
    x: number;
    y: number;
    cols: number;
    rows: number;
}
export declare class LayoutImpl implements Layout {
    cls: ClassImpl;
    ctrl: Control;
    cols: ColumnProps[];
    rows: RowProps[];
    items: LayoutItem[];
    x: number;
    y: number;
    constructor(cls: ClassImpl, ctrl: Control, cols?: ColumnProps[], rows?: RowProps[]);
    add(c: Control, p?: ControlProps): LayoutImpl;
    addAll(c: Control[]): LayoutImpl;
    skip(x?: number, y?: number): LayoutImpl;
    to(x: number, y: number): LayoutImpl;
    endl(): LayoutImpl;
}
export interface Dep {
    c: ControlImpl;
    prop?: string;
}
export declare class ControlImpl implements Control {
    name: string;
    cType: gen.ControlType;
    className: string;
    props: any;
    cls: ClassImpl;
    l: LayoutImpl;
    childs: ControlImpl[];
    deps: Dep;
    needToBind: boolean;
    bindProp: string;
    constructor(name: string, cType: gen.ControlType, className?: string, props?: any);
    layout(cols?: ColumnProps[], rows?: RowProps[]): LayoutImpl;
    add(c: Control): ControlImpl;
    bind(prop?: string): Control;
    dep(c: Control, prop?: string): Control;
}
