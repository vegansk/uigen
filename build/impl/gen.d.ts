import ast = require('./ast');
export declare enum GeneratorType {
    QOOXDOO_TS = 0,
}
export declare enum ControlType {
    LABEL = 0,
    TEXT = 1,
    PASSWORD = 2,
    CHECKBOX = 3,
    RADIOGROUP = 4,
    RADIO = 5,
    BUTTON = 6,
    GROUPBOX = 7,
    DATE = 8,
    INTEGER = 9,
    IPV4 = 10,
    FLOAT = 11,
    FRACTION = 12,
    MONEYSUM = 13,
    TIME = 14,
    PANEL = 15,
    COMBOBOX = 16,
    REFERENCE = 17,
    SEPARATOR = 18,
    TREE = 19,
    CHECKBOXTREE = 20,
    TEXTAREA = 21,
    TABVIEW = 22,
    TABLE = 23,
    SPINNER = 24,
    CARDVIEW = 25,
    SCROLLPANE = 26,
    CUSTOM = 27,
}
export interface ControlClasses {
    [index: string]: any;
}
export interface GeneratorClasses {
    [index: string]: ControlClasses;
}
export declare var CONTROL_CLASSES: GeneratorClasses;
export declare function getClassName(t: GeneratorType, c: ast.ControlImpl): string;
export interface SourceParams {
    tabWidth: number;
    gridHGap?: string;
    gridVGap?: string;
}
export declare var DEFAULT_SOURCE_PARAMS: SourceParams;
