var gen = require('./gen');
var ModuleImpl = (function () {
    function ModuleImpl(name) {
        this.name = name;
        this.classes = {};
    }
    ModuleImpl.prototype.setParams = function (p) {
        this.params = p;
        return this;
    };
    ModuleImpl.prototype.cls = function (name) {
        if (this.classes[name])
            throw new Error('Attempt to redefine class ' + name + ' in module ' + this.name);
        this.classes[name] = new ClassImpl(name, this);
        return this.classes[name];
    };
    return ModuleImpl;
})();
exports.ModuleImpl = ModuleImpl;
(function (ValidatorType) {
    ValidatorType[ValidatorType["NOT_EMPTY"] = 1] = "NOT_EMPTY";
    ValidatorType[ValidatorType["NOT_ZERO"] = 2] = "NOT_ZERO";
    ValidatorType[ValidatorType["NOT_EMPTY_INC"] = 3] = "NOT_EMPTY_INC";
    ValidatorType[ValidatorType["NOT_ZERO_INC"] = 4] = "NOT_ZERO_INC";
})(exports.ValidatorType || (exports.ValidatorType = {}));
var ValidatorType = exports.ValidatorType;
var ClassImpl = (function () {
    function ClassImpl(name, mod) {
        this.name = name;
        this.mod = mod;
        this.controls = {};
        this.bindProps = [];
        this.validators = [];
    }
    ClassImpl.prototype.add = function (c) {
        var ctrl = c;
        if (this.controls[ctrl.name] && this.controls[ctrl.name] !== ctrl)
            throw new Error('Attempt to redefine control ' + ctrl.name + ' in class ' + this.name + ', module ' + this.mod.name);
        this.controls[ctrl.name] = ctrl;
        ctrl.cls = this;
        return this;
    };
    ClassImpl.prototype.get = function (name) {
        return this.controls[name];
    };
    ClassImpl.prototype.layout = function (cols, rows) {
        if (this.l)
            throw new Error('Layout already set in class ' + this.name + ', module ' + this.mod.name);
        this.l = new LayoutImpl(this, this, cols, rows);
        return this.l;
    };
    ClassImpl.prototype.bind = function (prop) {
        if (!prop)
            throw new Error('Cannot bind class ' + this.mod.name + '.' + this.name + ' to itself');
        if (this.bindProps.indexOf(prop) >= 0)
            return;
        this.bindProps.push(prop);
        return this;
    };
    ClassImpl.prototype.dep = function (c, prop) {
        throw new Error('Class cannot be dependet on the other control');
    };
    ClassImpl.prototype.hasBind = function () {
        return this.bindProps.length !== 0 || this.getBindControls().length !== 0;
    };
    ClassImpl.prototype.getBindControls = function () {
        var _this = this;
        return Object.keys(this.controls).map(function (k) { return _this.controls[k]; }).filter(function (v) { return v.needToBind; });
    };
    ClassImpl.prototype.hasUiBind = function () {
        return this.getUiBind().length !== 0;
    };
    ClassImpl.prototype.getUiBind = function () {
        var _this = this;
        return Object.keys(this.controls).map(function (k) { return _this.controls[k]; }).filter(function (v) { return v.deps !== null; });
    };
    ClassImpl.prototype.notEmpty = function (v, msg) {
        this.validators.push({ t: 1 /* NOT_EMPTY */, v: v, msg: msg });
        return this;
    };
    ClassImpl.prototype.notZero = function (v, msg) {
        this.validators.push({ t: 2 /* NOT_ZERO */, v: v, msg: msg });
        return this;
    };
    ClassImpl.prototype.notEmptyInc = function (inc, v, msg) {
        this.validators.push({ t: 3 /* NOT_EMPTY_INC */, inc: inc, v: v, msg: msg });
        return this;
    };
    ClassImpl.prototype.notZeroInc = function (inc, v, msg) {
        this.validators.push({ t: 4 /* NOT_ZERO_INC */, inc: inc, v: v, msg: msg });
        return this;
    };
    return ClassImpl;
})();
exports.ClassImpl = ClassImpl;
var LayoutImpl = (function () {
    function LayoutImpl(cls, ctrl, cols, rows) {
        this.cls = cls;
        this.ctrl = ctrl;
        this.cols = cols;
        this.rows = rows;
        this.items = [];
        this.x = 0;
        this.y = 0;
    }
    LayoutImpl.prototype.add = function (c, p) {
        var i = {
            c: c,
            x: this.x,
            y: this.y,
            cols: p && p.colSpan ? p.colSpan : 1,
            rows: p && p.rowSpan ? p.rowSpan : 1
        };
        this.cls.add(i.c);
        this.items.push(i);
        this.x = this.x + i.cols;
        return this;
    };
    LayoutImpl.prototype.addAll = function (c) {
        var _this = this;
        c.forEach(function (ctrl) {
            _this.add(ctrl);
        });
        return this;
    };
    LayoutImpl.prototype.skip = function (x, y) {
        if (!x && !y)
            x = 1;
        this.x = this.x + (x ? x : 0);
        this.y = this.y + (y ? y : 0);
        return this;
    };
    LayoutImpl.prototype.to = function (x, y) {
        this.x = x;
        this.y = y;
        return this;
    };
    LayoutImpl.prototype.endl = function () {
        this.x = 0;
        this.y = this.y + 1;
        return this;
    };
    return LayoutImpl;
})();
exports.LayoutImpl = LayoutImpl;
var ControlImpl = (function () {
    function ControlImpl(name, cType, className, props) {
        this.name = name;
        this.cType = cType;
        this.className = className;
        this.props = props;
        this.childs = [];
        this.deps = null;
        this.needToBind = false;
        this.bindProp = null;
    }
    ControlImpl.prototype.layout = function (cols, rows) {
        var cDesc = gen.ControlType[this.cType] + '[' + this.name + ']';
        if (!this.cls)
            throw new Error('Can\'t create layout for control ' + cDesc + ' without class');
        if (this.l)
            throw new Error('Attempt to redefine control ' + cDesc + ' layout');
        this.l = new LayoutImpl(this.cls, this, cols, rows);
        return this.l;
    };
    ControlImpl.prototype.add = function (c) {
        var cimpl = c;
        if (!this.cls) {
            throw new Error('Can\'t add control ' + gen.ControlType[cimpl.cType] + '[' + cimpl.name + '] to the parent ' + gen.ControlType[this.cType] + '[' + this.name + ']  without class');
        }
        this.childs.push(cimpl);
        this.cls.add(cimpl);
        return this;
    };
    ControlImpl.prototype.bind = function (prop) {
        if (!prop && this.cType !== 27 /* CUSTOM */)
            throw new Error('Can\'t bind control ' + this.name + ' of type ' + gen.ControlType[this.cType] + ' to the parent\'s model');
        this.bindProp = prop || null;
        this.needToBind = true;
        return this;
    };
    ControlImpl.prototype.dep = function (c, prop) {
        this.deps = { c: c, prop: prop };
        return this;
    };
    return ControlImpl;
})();
exports.ControlImpl = ControlImpl;
//# sourceMappingURL=ast.js.map