var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var ast = require('./ast');
var gen = require('./gen');
var ReadOnlyType;
(function (ReadOnlyType) {
    ReadOnlyType[ReadOnlyType["READ_ONLY"] = 1] = "READ_ONLY";
    ReadOnlyType[ReadOnlyType["ENABLED"] = 2] = "ENABLED";
})(ReadOnlyType || (ReadOnlyType = {}));
function lbl(name) {
    return name !== null && name !== undefined ? "'" + name + "'" : "null";
}
var CONTROL_SPECS = {
    'LABEL': {
        ctorParams: function (p) {
            return lbl(p.label);
        },
        propsFlt: ['label']
    },
    'TEXT': {
        readOnly: 1 /* READ_ONLY */,
        bindProp: 'value'
    },
    'PASSWORD': {
        readOnly: 1 /* READ_ONLY */,
        bindProp: 'value'
    },
    'CHECKBOX': {
        ctorParams: function (p) {
            return lbl(p.label);
        },
        propsFlt: ['label'],
        readOnly: 2 /* ENABLED */,
        bindProp: 'value'
    },
    'RADIOGROUP': {
        bindProp: 'rbValue'
    },
    'RADIO': {
        ctorParams: function (p) {
            return lbl(p.label);
        },
        propsFlt: ['label'],
        readOnly: 2 /* ENABLED */,
        bindProp: 'value'
    },
    'BUTTON': {
        ctorParams: function (p) {
            return lbl(p.label) + (p.icon ? ', \'' + p.icon + '\'' : '');
        },
        propsFlt: ['label', 'icon'],
        readOnly: 2 /* ENABLED */
    },
    'GROUPBOX': {
        ctorParams: function (p) {
            return lbl(p.label);
        },
        propsFlt: ['label']
    },
    'DATE': {
        readOnly: 1 /* READ_ONLY */,
        bindProp: 'value'
    },
    'INTEGER': {
        readOnly: 1 /* READ_ONLY */,
        bindProp: 'intValue'
    },
    'IPV4': {
        readOnly: 1 /* READ_ONLY */,
        bindProp: 'value'
    },
    'FLOAT': {
        readOnly: 1 /* READ_ONLY */,
        bindProp: 'floatValue'
    },
    'FRACTION': {
        readOnly: 1 /* READ_ONLY */,
        bindProp: 'fractionValue'
    },
    'MONEYSUM': {
        readOnly: 1 /* READ_ONLY */,
        bindProp: 'moneyValue'
    },
    'TIME': {
        readOnly: 1 /* READ_ONLY */,
        bindProp: 'value'
    },
    'PANEL': {
        ctorParams: function (_) {
            return 'null';
        }
    },
    'COMBOBOX': {
        readOnly: 2 /* ENABLED */,
        bindProp: 'value'
    },
    'REFERENCE': {
        readOnly: 2 /* ENABLED */,
        bindProp: 'value'
    },
    'SEPARATOR': {
        ctorParams: function (p) {
            return lbl(p.label);
        },
        propsFlt: ['label']
    },
    'TREE': {
        readOnly: 2 /* ENABLED */,
        bindProp: 'selectedValue'
    },
    'CHECKBOXTREE': {
        readOnly: 2 /* ENABLED */,
        bindProp: 'checkedValues'
    },
    'TEXTAREA': {
        readOnly: 1 /* READ_ONLY */,
        bindProp: 'value'
    },
    'TABLE': {
        propsFlt: ['scrollable']
    },
    'SPINNER': {
        ctorParams: function (p) {
            return '' + (p.min || 0) + ', ' + (p.value || 0) + ', ' + (p.max || 0);
        },
        propsFlt: ['min', 'max', 'value'],
        readOnly: 2 /* ENABLED */
    },
    'CUSTOM': {
        readOnly: 1 /* READ_ONLY */
    }
};
var GeneratorBase = (function () {
    function GeneratorBase(p, lvl, out) {
        this.p = p;
        this.lvl = lvl;
        this.out = out;
    }
    GeneratorBase.prototype.tab = function () {
        var t = '';
        for (var i = 0; i < this.lvl * this.p.tabWidth; i++)
            t += ' ';
        return t;
    };
    GeneratorBase.prototype.line = function (s) {
        if (s === void 0) { s = ''; }
        var data = '';
        if (!s) {
            data += '\n';
        }
        else {
            data += this.tab();
            data += s + '\n';
        }
        this.out(data);
    };
    GeneratorBase.prototype.jsValue = function (v) {
        return JSON.stringify(v);
    };
    GeneratorBase.prototype.getterName = function (prop) {
        var res = '';
        prop.split('.').forEach(function (v) {
            if (res !== '')
                res += '.';
            res += 'get' + v.charAt(0).toUpperCase() + v.substring(1) + '()';
        });
        return res;
    };
    return GeneratorBase;
})();
var LayoutGenerator = (function (_super) {
    __extends(LayoutGenerator, _super);
    function LayoutGenerator(l, idx, p, lvl, out) {
        _super.call(this, p, lvl, out);
        this.l = l;
        this.idx = idx;
    }
    LayoutGenerator.prototype.generateLayout = function () {
        this.line('var ' + this.varName() + ' = new qx.ui.layout.Grid(' + this.p.gridHGap + ', ' + this.p.gridVGap + ')');
        this.callSetLayout();
        var cols = this.l.cols || [];
        var rows = this.l.rows || [];
        cols.forEach(this.generateColSpec, this);
        rows.forEach(this.generateRowSpec, this);
        this.l.items.forEach(this.generateItemSpec, this);
    };
    LayoutGenerator.prototype.generateColSpec = function (c, idx) {
        if (!c)
            return;
        if (c.flex)
            this.line(this.varName() + '.setColumnFlex(' + idx + ', ' + c.flex + ')');
        if (c.width)
            this.line(this.varName() + '.setColumnWidth(' + idx + ', ' + c.width + ')');
        if (c.alignX || c.alignY)
            this.line(this.varName() + '.setColumnAlign(' + idx + ', ' + this.jsValue(c.alignX || 'right') + ', ' + this.jsValue(c.alignY || 'middle') + ')');
    };
    LayoutGenerator.prototype.generateRowSpec = function (r, idx) {
        if (!r)
            return;
        if (r.flex)
            this.line(this.varName() + '.setRowFlex(' + idx + ', ' + r.flex + ')');
        if (r.height)
            this.line(this.varName() + '.setRowHeight(' + idx + ', ' + r.height + ')');
        if (r.alignX || r.alignY)
            this.line(this.varName() + '.setRowAlign(' + idx + ', ' + this.jsValue(r.alignX || 'right') + ', ' + this.jsValue(r.alignY || 'middle') + ')');
    };
    LayoutGenerator.prototype.generateItemSpec = function (i) {
        this.callAddItem(i);
    };
    LayoutGenerator.prototype.callSetLayout = function () {
        if (this.l.ctrl === this.l.cls) {
            this.line('this._setLayout(' + this.varName() + ')');
        }
        else {
            this.line('this.' + this.l.ctrl.name + '.setLayout(' + this.varName() + ')');
        }
    };
    LayoutGenerator.prototype.callAddItem = function (i) {
        if (this.l.ctrl === this.l.cls) {
            this.line('this._add(this.' + i.c.name + ', { row: ' + i.y + ', column: ' + i.x + ', rowSpan: ' + i.rows + ', colSpan: ' + i.cols + '})');
        }
        else {
            this.line('this.' + this.l.ctrl.name + '.add(this.' + i.c.name + ', { row: ' + i.y + ', column: ' + i.x + ', rowSpan: ' + i.rows + ', colSpan: ' + i.cols + '})');
        }
    };
    LayoutGenerator.prototype.varName = function () {
        return 'layout' + (this.idx === 0 ? '' : '' + this.idx);
    };
    return LayoutGenerator;
})(GeneratorBase);
var ClassGenerator = (function (_super) {
    __extends(ClassGenerator, _super);
    function ClassGenerator(cls, p, lvl, out) {
        _super.call(this, p, lvl, out);
        this.cls = cls;
        this.tabPageIdx = 0;
    }
    ClassGenerator.prototype.generateClass = function () {
        this.line('export class ' + this.cls.name + ' extends qx.ui.core.Widget implements tslib.ui.ReadOnly, tslib.ui.Validatable {');
        this.line();
        this.lvl++;
        var hasBind = this.cls.hasBind();
        if (hasBind) {
            this.line('bindCtrl: qx.data.controller.Object = null');
            this.line('bindModel: any = null');
            this.line();
        }
        for (var ctrl in this.cls.controls) {
            var c = this.cls.controls[ctrl];
            var className = gen.getClassName(0 /* QOOXDOO_TS */, c);
            if (className.match(/^eldisjs\./))
                className = 'any';
            this.line(c.name + ': ' + className);
        }
        this.line();
        this.line('constructor() {');
        this.lvl++;
        this.line('super()');
        this.line('this.initControls()');
        if (this.cls.l)
            this.line('this.setupLayout()');
        this.lvl--;
        this.line('}');
        this.line();
        this.generateInitControls();
        if (this.cls.l)
            this.generateLayout();
        this.generateReadOnly();
        if (hasBind) {
            this.generateSetupModel();
            this.generateInitBindings();
            this.generateGetModel();
            this.generateSetModel();
            this.genetateCleanup();
        }
        this.generateInitUiBindings();
        this.generateValidate();
        this.lvl--;
        this.line('}');
    };
    ClassGenerator.prototype.generateInitControls = function () {
        var _this = this;
        this.line('private initControls() {');
        this.lvl++;
        Object.keys(this.cls.controls).map(function (n) {
            return _this.cls.controls[n];
        }).forEach(function (c) {
            var spec = CONTROL_SPECS[gen.ControlType[c.cType]] || {
                ctorParams: function () {
                    return '';
                }
            };
            _this.generateCreateControl(c, spec);
        });
        Object.keys(this.cls.controls).map(function (n) {
            return _this.cls.controls[n];
        }).filter(function (c) {
            return c.childs.length !== 0;
        }).forEach(function (c) {
            _this.generateAddChilds(c);
        });
        this.lvl--;
        this.line('}');
        this.line();
    };
    ClassGenerator.prototype.generateReadOnly = function () {
        var _this = this;
        this.line('private __gen_isReadOnly = false');
        this.line();
        this.line('isReadOnly(): boolean {');
        this.lvl++;
        this.line('return this.__gen_isReadOnly');
        this.lvl--;
        this.line('}');
        this.line();
        this.line('setReadOnly(readOnly: boolean) {');
        this.lvl++;
        this.line('this.__gen_isReadOnly = readOnly;');
        Object.keys(this.cls.controls).map(function (n) { return _this.cls.controls[n]; }).forEach(function (c) {
            var spec = CONTROL_SPECS[gen.ControlType[c.cType]];
            if (!spec || !spec.readOnly)
                return;
            switch (spec.readOnly) {
                case 1 /* READ_ONLY */:
                    _this.line('this.' + c.name + '.setReadOnly(readOnly);');
                    break;
                case 2 /* ENABLED */:
                    _this.line('this.' + c.name + '.setEnabled(!readOnly);');
                    break;
            }
        });
        this.lvl--;
        this.line('}');
        this.line();
    };
    ClassGenerator.prototype.generateAddChilds = function (c) {
        var _this = this;
        c.childs.forEach(function (ch) {
            if (c.cType === 22 /* TABVIEW */) {
                _this.tabPageIdx++;
                _this.line('var tabPage' + _this.tabPageIdx + ' = new qx.ui.tabview.Page(' + _this.jsValue(ch.props['tabLabel']) + ', null)');
                _this.line('tabPage' + _this.tabPageIdx + '.setLayout(new qx.ui.layout.Canvas())');
                _this.line('tabPage' + _this.tabPageIdx + '.add(this.' + ch.name + ', { edge: 0 })');
                _this.line('this.' + c.name + '.add(tabPage' + _this.tabPageIdx + ')');
            }
            else if (c.cType === 25 /* CARDVIEW */) {
                _this.line('this.' + c.name + '.addItem(this.' + ch.name + ', ' + _this.jsValue(ch.props['cardName']) + ')');
            }
            else if (c.cType === 4 /* RADIOGROUP */) {
                _this.line('this.' + c.name + '.addItem(this.' + ch.name + ', ' + _this.jsValue(ch.props['rbValue']) + ')');
            }
            else
                _this.line('this.' + c.name + '.add(this.' + ch.name + ')');
        });
    };
    ClassGenerator.prototype.generateCreateControl = function (c, s) {
        var l = 'this.' + c.name + ' = new ' + gen.getClassName(0 /* QOOXDOO_TS */, c) + '(' + (s.ctorParams ? s.ctorParams(c.props) : '') + ')';
        var p = {};
        if (c.props) {
            if ((typeof c.props) !== 'object')
                throw new Error('Control ' + c.name + ' contains bad props of type ' + (typeof c.props));
            for (var pn in c.props || {}) {
                if ((s.propsFlt && s.propsFlt.indexOf(pn) !== -1) || pn === 'tabLabel' || pn === 'cardName' || pn === 'rbValue')
                    continue;
                p[pn] = c.props[pn];
            }
        }
        this.line(l + (Object.keys(p).length !== 0 ? '.set({' : ''));
        if (Object.keys(p).length !== 0) {
            this.lvl++;
            var pc = Object.keys(p).length;
            for (pn in p) {
                pc--;
                this.line(pn + ': ' + this.jsValue(p[pn]) + (pc == 0 ? '' : ','));
            }
            this.lvl--;
            this.line('})');
        }
    };
    ClassGenerator.prototype.generateSetupModel = function () {
        var _this = this;
        this.line('setupModel(m: any): any {');
        this.lvl++;
        this.line('if(!m) throw new Error(\'' + this.cls.name + '.setupModel called with undefined model\')');
        this.line();
        var generatedObjects = [];
        this.cls.getBindControls().forEach(function (v) {
            _this.generateControlModel(v.bindProp, v.cType === 20 /* CHECKBOXTREE */ ? '[]' : 'null', generatedObjects, v);
        });
        this.cls.bindProps.forEach(function (v) { return _this.generateControlModel(v, 'null', generatedObjects); });
        this.line();
        this.line('return m');
        this.lvl--;
        this.line('}');
        this.line();
    };
    ClassGenerator.prototype.generateControlModel = function (name, initializer, generated, c) {
        var _this = this;
        if (c && c.cType === 27 /* CUSTOM */ && !name) {
            this.line('this.' + c.name + '.setupModel(m)');
            return;
        }
        var genInitializer = function (path, initializer) {
            return "(m." + path + " === null || m." + path + " === undefined) ? " + initializer + " : m." + path;
        };
        var path = null;
        var spl = name.split('.');
        var curr = 0;
        spl.forEach(function (n) {
            curr++;
            path = (path === null ? '' : path + '.') + n;
            if (generated.indexOf(path) >= 0)
                return;
            if (curr === spl.length && c && c.cType === 27 /* CUSTOM */ && name !== null) {
                _this.line("m." + path + " = this." + c.name + ".setupModel(" + genInitializer(path, '{}') + ")");
            }
            else {
                _this.line("m." + path + " = " + genInitializer(path, curr === spl.length ? initializer : '{}'));
            }
            generated.push(path);
        });
    };
    ClassGenerator.prototype.generateCreateBindModel = function () {
        this.line('createBindModel(jsonModel: any) {');
        this.lvl++;
        this.line('this.bindModel = qx.data.marshal.Json.createModel(jsonModel, false)');
        this.lvl--;
        this.line('}');
        this.line();
    };
    ClassGenerator.prototype.generateInitBindings = function () {
        var _this = this;
        this.line('initBindings(qxModel: any) {');
        this.lvl++;
        this.line('this.bindModel = qxModel');
        this.line('this.bindCtrl = new qx.data.controller.Object(qxModel)');
        this.line();
        this.cls.getBindControls().forEach(function (v) {
            if (v.cType === 27 /* CUSTOM */) {
                _this.line('this.' + v.name + '.initBindings(' + (v.bindProp ? 'this.bindModel.' + _this.getterName(v.bindProp) : 'this.bindModel') + ')');
                return;
            }
            var spec = CONTROL_SPECS[gen.ControlType[v.cType]];
            if (!spec || !spec.bindProp)
                return;
            _this.line('this.bindCtrl.addTarget(this.' + v.name + ', "' + spec.bindProp + '", "' + v.bindProp + '", true)');
        });
        this.lvl--;
        this.line('}');
        this.line();
    };
    ClassGenerator.prototype.genetateCleanup = function () {
        var _this = this;
        this.line('cleanupModel(m: any) {');
        this.lvl++;
        this.cls.getUiBind().filter(function (c) { return c.bindProp !== null; }).forEach(function (c) {
            var d = c.deps;
            var spec = CONTROL_SPECS[gen.ControlType[d.c.cType]] || { bindProp: null };
            var value = c.deps.prop ? c.deps.prop : (spec.bindProp ? spec.bindProp : null);
            if (!value)
                return;
            value = 'get' + value[0].toUpperCase() + value.slice(1);
            _this.line('if(!this.' + c.deps.c.name + '.' + value + '()) delete m.' + c.bindProp);
        });
        this.line('return m');
        this.lvl--;
        this.line('}');
        this.line();
    };
    ClassGenerator.prototype.generateGetModel = function () {
        var _this = this;
        this.line('getModel(parentModel?: any): any {');
        this.lvl++;
        this.line('if(!this.bindModel) throw new Error(\'' + this.cls.name + '.getModel has no model\')');
        this.line();
        this.line('var m = <any>qx.util.Serializer.toNativeObject(this.bindModel, null, null)');
        this.cls.getBindControls().filter(function (_) { return _.cType === 27 /* CUSTOM */ && !_.bindProp; }).forEach(function (c) { return _this.line('this.' + c.name + '.getModel(m)'); });
        this.line();
        this.line('if(parentModel !== undefined) {');
        this.lvl++;
        this.line('tslib.core.extendObject(parentModel, m, false, true)');
        this.line('m = parentModel');
        this.lvl--;
        this.line('}');
        this.line();
        var c = this.cls.getBindControls().filter(function (_) { return _.cType === 27 /* CUSTOM */ && !!_.bindProp; });
        c.forEach(function (c) { return _this.line('m.' + c.bindProp + ' = this.' + c.name + '.getModel()'); });
        if (c.length !== 0)
            this.line();
        this.line('m = this.cleanupModel(m)');
        this.line();
        this.line('return m');
        this.lvl--;
        this.line('}');
        this.line();
    };
    ClassGenerator.prototype.generateSetModel = function () {
        var _this = this;
        this.line('setModel(m: any): void {');
        this.lvl++;
        this.line('var m = this.setupModel(m || {})');
        this.line('this.initBindings(qx.data.marshal.Json.createModel(m, false))');
        this.line('this.initUiBindings()');
        this.line();
        this.cls.getUiBind().filter(function (c) { return c.bindProp !== null; }).forEach(function (c) {
            var d = c.deps;
            var spec = CONTROL_SPECS[gen.ControlType[d.c.cType]] || { bindProp: null };
            var value = c.deps.prop ? c.deps.prop : (spec.bindProp ? spec.bindProp : null);
            if (!value)
                return;
            value = 'set' + value[0].toUpperCase() + value.slice(1);
            _this.line('this.' + d.c.name + '.' + value + '(!(!m.' + c.bindProp + ' || (Array.isArray(m.' + c.bindProp + ') && m.' + c.bindProp + '.length === 0)))');
        });
        this.lvl--;
        this.line('}');
        this.line();
    };
    ClassGenerator.prototype.generateInitUiBindings = function () {
        var _this = this;
        this.line('initUiBindings(): void {');
        this.lvl++;
        this.cls.getUiBind().forEach(function (c) {
            var d = c.deps;
            var spec = CONTROL_SPECS[gen.ControlType[d.c.cType]] || { bindProp: null };
            var value = c.deps.prop ? c.deps.prop : (spec.bindProp ? spec.bindProp : null);
            if (!value)
                return;
            _this.line('this.' + d.c.name + '.bind("' + value + '", this.' + c.name + ', "' + (c.cType === 25 /* CARDVIEW */ ? 'selectedItem' : 'enabled') + '", null)');
        });
        this.cls.getBindControls().filter(function (_) { return _.cType === 27 /* CUSTOM */; }).forEach(function (_) { return _this.line('this.' + _.name + '.initUiBindings()'); });
        this.lvl--;
        this.line('}');
        this.line();
    };
    ClassGenerator.prototype.generateValidate = function () {
        var _this = this;
        this.line('validate(): tslib.core.Promise<boolean> {');
        this.lvl++;
        var v = this.cls.validators;
        if (v.length === 0) {
            this.line('return tslib.core.mkPromise(true)');
        }
        else {
            this.line('return tslib.ui.validator()');
            this.lvl++;
            v.forEach(function (v) {
                switch (v.t) {
                    case 1 /* NOT_EMPTY */:
                        _this.line('.notEmpty(this.' + v.v.name + ', ' + _this.jsValue(v.msg) + ')');
                        break;
                    case 2 /* NOT_ZERO */:
                        _this.line('.notZero(this.' + v.v.name + ', ' + _this.jsValue(v.msg) + ')');
                        break;
                    case 3 /* NOT_EMPTY_INC */:
                        _this.line('.notEmptyInc(this.' + v.inc.name + ', this.' + v.v.name + ', ' + _this.jsValue(v.msg) + ')');
                        break;
                    case 4 /* NOT_ZERO_INC */:
                        _this.line('.notZeroInc(this.' + v.inc.name + ', this.' + v.v.name + ', ' + _this.jsValue(v.msg) + ')');
                        break;
                    default:
                        throw new Error('Unknown validator type ' + ast.ValidatorType[v.t]);
                }
            });
            this.line('.validate()');
            this.lvl--;
        }
        this.lvl--;
        this.line('}');
        this.line();
    };
    ClassGenerator.prototype.generateLayout = function () {
        var _this = this;
        this.line('private setupLayout() {');
        this.lvl++;
        new LayoutGenerator(this.cls.l, 0, this.p, this.lvl, this.out).generateLayout();
        var idx = 1;
        Object.keys(this.cls.controls).map(function (n) {
            return _this.cls.controls[n];
        }).filter(function (c) {
            return !!c.l;
        }).forEach(function (c) {
            _this.line();
            new LayoutGenerator(c.l, idx, _this.p, _this.lvl, _this.out).generateLayout();
            idx++;
        });
        this.lvl--;
        this.line('}');
        this.line();
    };
    return ClassGenerator;
})(GeneratorBase);
var ModuleGenerator = (function (_super) {
    __extends(ModuleGenerator, _super);
    function ModuleGenerator(m, p, out) {
        _super.call(this, p, 0, out);
        this.m = m;
        this.data = '';
    }
    ModuleGenerator.prototype.generateModule = function () {
        this.lvl = 0;
        this.generateImport();
        this.line('module ' + this.m.name + ' {');
        this.line();
        this.lvl++;
        for (var c in this.m.classes) {
            this.generateClass(this.m.classes[c]);
        }
        this.lvl--;
        this.line('}');
    };
    ModuleGenerator.prototype.generateImport = function () {
        if (this.m.params && this.m.params.qxImport) {
            this.line('/// <reference path="' + this.m.params.qxImport + '" />');
            this.line();
        }
    };
    ModuleGenerator.prototype.generateClass = function (cls) {
        new ClassGenerator(cls, this.p, this.lvl, this.out).generateClass();
    };
    return ModuleGenerator;
})(GeneratorBase);
function generate(o, p, out) {
    var g = new ModuleGenerator(o, p, out);
    g.generateModule();
}
module.exports = generate;
//# sourceMappingURL=qooxdoo_ts.js.map