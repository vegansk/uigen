(function (GeneratorType) {
    GeneratorType[GeneratorType["QOOXDOO_TS"] = 0] = "QOOXDOO_TS";
})(exports.GeneratorType || (exports.GeneratorType = {}));
var GeneratorType = exports.GeneratorType;
(function (ControlType) {
    ControlType[ControlType["LABEL"] = 0] = "LABEL";
    ControlType[ControlType["TEXT"] = 1] = "TEXT";
    ControlType[ControlType["PASSWORD"] = 2] = "PASSWORD";
    ControlType[ControlType["CHECKBOX"] = 3] = "CHECKBOX";
    ControlType[ControlType["RADIOGROUP"] = 4] = "RADIOGROUP";
    ControlType[ControlType["RADIO"] = 5] = "RADIO";
    ControlType[ControlType["BUTTON"] = 6] = "BUTTON";
    ControlType[ControlType["GROUPBOX"] = 7] = "GROUPBOX";
    ControlType[ControlType["DATE"] = 8] = "DATE";
    ControlType[ControlType["INTEGER"] = 9] = "INTEGER";
    ControlType[ControlType["IPV4"] = 10] = "IPV4";
    ControlType[ControlType["FLOAT"] = 11] = "FLOAT";
    ControlType[ControlType["FRACTION"] = 12] = "FRACTION";
    ControlType[ControlType["MONEYSUM"] = 13] = "MONEYSUM";
    ControlType[ControlType["TIME"] = 14] = "TIME";
    ControlType[ControlType["PANEL"] = 15] = "PANEL";
    ControlType[ControlType["COMBOBOX"] = 16] = "COMBOBOX";
    ControlType[ControlType["REFERENCE"] = 17] = "REFERENCE";
    ControlType[ControlType["SEPARATOR"] = 18] = "SEPARATOR";
    ControlType[ControlType["TREE"] = 19] = "TREE";
    ControlType[ControlType["CHECKBOXTREE"] = 20] = "CHECKBOXTREE";
    ControlType[ControlType["TEXTAREA"] = 21] = "TEXTAREA";
    ControlType[ControlType["TABVIEW"] = 22] = "TABVIEW";
    ControlType[ControlType["TABLE"] = 23] = "TABLE";
    ControlType[ControlType["SPINNER"] = 24] = "SPINNER";
    ControlType[ControlType["CARDVIEW"] = 25] = "CARDVIEW";
    ControlType[ControlType["SCROLLPANE"] = 26] = "SCROLLPANE";
    ControlType[ControlType["CUSTOM"] = 27] = "CUSTOM";
})(exports.ControlType || (exports.ControlType = {}));
var ControlType = exports.ControlType;
exports.CONTROL_CLASSES = {
    'QOOXDOO_TS': {
        'LABEL': 'qx.ui.basic.Label',
        'TEXT': 'tslib.ui.TextField',
        'PASSWORD': 'qx.ui.form.PasswordField',
        'CHECKBOX': 'qx.ui.form.CheckBox',
        'RADIOGROUP': 'tslib.ui.RadioGroup',
        'RADIO': 'qx.ui.form.RadioButton',
        'BUTTON': 'qx.ui.form.Button',
        'GROUPBOX': 'qx.ui.groupbox.GroupBox',
        'DATE': 'tslib.ui.DateField',
        'INTEGER': 'tslib.ui.IntegerField',
        'IPV4': 'tslib.ui.IpV4Field',
        'FLOAT': 'tslib.ui.FloatField',
        'FRACTION': 'tslib.ui.FractionField',
        'MONEYSUM': 'tslib.ui.MoneyField',
        'TIME': 'tslib.ui.TimeField',
        'PANEL': 'qx.ui.container.Composite',
        'COMBOBOX': 'qx.ui.form.ComboBox',
        'REFERENCE': 'tslib.ui.ReferenceBox',
        'SEPARATOR': 'tslib.ui.Separator',
        'TREE': 'tslib.ui.Tree',
        'CHECKBOXTREE': 'tslib.ui.CheckBoxTree',
        'TEXTAREA': 'tslib.ui.TextArea',
        'TABVIEW': 'qx.ui.tabview.TabView',
        'TABLE': function (props) {
            if (props.scrollable)
                return 'tslib.ui.ScrollableTable';
            else
                return 'tslib.ui.Table';
        },
        'SPINNER': 'qx.ui.form.Spinner',
        'CARDVIEW': 'tslib.ui.CardPanel',
        'SCROLLPANE': 'qx.ui.container.Scroll'
    }
};
function getClassName(t, c) {
    if (c.cType === 27 /* CUSTOM */)
        return c.className;
    var cn = exports.CONTROL_CLASSES[GeneratorType[t]][ControlType[c.cType]];
    if (!cn)
        throw new Error('Cannot find class name for generator ' + GeneratorType[t] + ', control type ' + ControlType[c.cType]);
    if (typeof cn === 'string')
        return cn;
    return cn(c.props);
}
exports.getClassName = getClassName;
exports.DEFAULT_SOURCE_PARAMS = {
    tabWidth: 2,
    gridHGap: '5',
    gridVGap: '5'
};
//# sourceMappingURL=gen.js.map