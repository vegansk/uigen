var ast = require('./impl/ast');
var gen = require('./impl/gen');
var qooxdooGenerate = require('./impl/qooxdoo_ts');
function mod(name) {
    return new ast.ModuleImpl(name);
}
exports.mod = mod;
function custom(name, className, props) {
    return new ast.ControlImpl(name, 27 /* CUSTOM */, className, props);
}
exports.custom = custom;
function createControl(name, cType, props) {
    props = props || {};
    if (cType == 27 /* CUSTOM */)
        throw new Error('Use "custom()" function to create controls of that type.');
    return new ast.ControlImpl(name, cType, null, props);
}
function label(name, label, props) {
    props = props || {};
    props.alignY = props.alignY || 'middle';
    props.label = label;
    return createControl(name, 0 /* LABEL */, props);
}
exports.label = label;
function text(name, props) {
    return createControl(name, 1 /* TEXT */, props);
}
exports.text = text;
function pass(name, props) {
    return createControl(name, 2 /* PASSWORD */, props);
}
exports.pass = pass;
function check(name, label, props) {
    props = props || {};
    props.label = label;
    return createControl(name, 3 /* CHECKBOX */, props);
}
exports.check = check;
function radio(name, label, props) {
    props = props || {};
    props.label = label;
    return createControl(name, 5 /* RADIO */, props);
}
exports.radio = radio;
function radioGroup(name, props) {
    return createControl(name, 4 /* RADIOGROUP */, props);
}
exports.radioGroup = radioGroup;
function button(name, label, icon, props) {
    props = props || {};
    props.label = label;
    props.icon = icon;
    return createControl(name, 6 /* BUTTON */, props);
}
exports.button = button;
function groupBox(name, label, props) {
    props = props || {};
    props.label = label;
    return createControl(name, 7 /* GROUPBOX */, props);
}
exports.groupBox = groupBox;
function date(name, props) {
    return createControl(name, 8 /* DATE */, props);
}
exports.date = date;
function integer(name, props) {
    return createControl(name, 9 /* INTEGER */, props);
}
exports.integer = integer;
function ipV4(name, props) {
    return createControl(name, 10 /* IPV4 */, props);
}
exports.ipV4 = ipV4;
function float(name, props) {
    return createControl(name, 11 /* FLOAT */, props);
}
exports.float = float;
function fraction(name, props) {
    return createControl(name, 12 /* FRACTION */, props);
}
exports.fraction = fraction;
function moneySum(name, props) {
    return createControl(name, 13 /* MONEYSUM */, props);
}
exports.moneySum = moneySum;
function time(name, props) {
    return createControl(name, 14 /* TIME */, props);
}
exports.time = time;
function panel(name, props) {
    return createControl(name, 15 /* PANEL */, props);
}
exports.panel = panel;
function combo(name, props) {
    return createControl(name, 16 /* COMBOBOX */, props);
}
exports.combo = combo;
function ref(name, props) {
    return createControl(name, 17 /* REFERENCE */, props);
}
exports.ref = ref;
function separator(name, label, props) {
    props = props || {};
    props.label = label;
    return createControl(name, 18 /* SEPARATOR */, props);
}
exports.separator = separator;
function tree(name, props) {
    return createControl(name, 19 /* TREE */, props);
}
exports.tree = tree;
function checkTree(name, props) {
    return createControl(name, 20 /* CHECKBOXTREE */, props);
}
exports.checkTree = checkTree;
function textArea(name, props) {
    return createControl(name, 21 /* TEXTAREA */, props);
}
exports.textArea = textArea;
function tabView(name, props) {
    return createControl(name, 22 /* TABVIEW */, props);
}
exports.tabView = tabView;
function table(name, scrollable, props) {
    props = props || {};
    props.scrollable = scrollable;
    return createControl(name, 23 /* TABLE */, props);
}
exports.table = table;
function spin(name, min, max, value, props) {
    props = props || {};
    props.min = min;
    props.max = max;
    props.value = value;
    return createControl(name, 24 /* SPINNER */, props);
}
exports.spin = spin;
function cardView(name, props) {
    return createControl(name, 25 /* CARDVIEW */, props);
}
exports.cardView = cardView;
function scrollPane(name, props) {
    return createControl(name, 26 /* SCROLLPANE */, props);
}
exports.scrollPane = scrollPane;
function genQooxdooTsModule(m, p) {
    var s = '';
    qooxdooGenerate(m, p ? p : gen.DEFAULT_SOURCE_PARAMS, function (d) {
        s += d;
    });
    return s;
}
exports.genQooxdooTsModule = genQooxdooTsModule;
//# sourceMappingURL=uigen.js.map