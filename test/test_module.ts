import uigen = require('../src/uigen')

var m = uigen.mod('test').setParams({
  qxImport: '../../stdafx.ts'
})
var c = m.cls('TestClass2')
var s = uigen.scrollPane('scrollPane')
var p = uigen.panel('testPanel')
var tab = uigen.tabView('testTab')
var card = uigen.cardView('cardView')

var rb1 = uigen.radio('rb1', 'Radio 1', { rbValue: 'name1'})
var rb2 = uigen.radio('rb2', 'Radio 2', { rbValue: 'name2'})

var rg = uigen.radioGroup('rg')
c.add(rg)
rg.add(rb1).add(rb2)

c.layout([{alignX: 'right'}, {flex: 1}])
  .add(uigen.tree('treeCtrl').bind('treeValue')).endl()
  .add(uigen.label('lbl1', 'Test:', {rich: true}))
  .add(uigen.fraction('ed1').bind('ed1Value'))
  .add(uigen.float('float1').bind('float1Value'))
  .endl()
  .add(uigen.label('lbl2', null))
  .add(uigen.check('chk1', 'EnableCheck'))
  .add(uigen.integer('ed2').bind('subObj.ed2Value').dep(c.get('chk1')))
  .add(uigen.spin('spin', 0, 10))
  .endl()
  .add(s, { colSpan: 2 })
  .add(rb1).add(rb2).endl()
  .add(tab).endl()
  .add(card)
  .add(uigen.table('testTable', false))
  .add(uigen.date('dateField'))
  .add(uigen.checkTree('checkTree'))
  .add(uigen.custom('childPanel', 'test.ChildPanel').bind('child.data'))
  .add(uigen.custom('inlinePanel', 'test.InlinePanel').bind())

c.get('checkTree').bind('subObj.subSubObj.checkTreeValue')

s.add(p)

p.layout()
  .add(uigen.label('lblHello', 'Hello, '))
  .add(uigen.check('chkWorld', 'World!'))
  .endl()
  .add(uigen.custom('customCtrl', 'org.vega.Lalala'))

tab
  .add(uigen.panel('tabPanel', { tabLabel: 'Test Panel' }))
  .add(uigen.panel('tabPanel2', { tabLabel: 'Test Panel 2' }))

card
  .add(uigen.panel('card1', { cardName: 'name1' }))
  .add(uigen.panel('card2', { cardName: 'name2' }))

card.dep(rg)

c.bind('this.is.model.that.not.used.in.initBindings').bind('this.is.model.that.not.used.in.initBindings2')

c.notEmptyInc(c.get('chk1'), c.get('ed1'), 'shit!!!').notZeroInc(c.get('ed2'), c.get('spin'))
  .notEmpty(c.get('ed1'), 'shit!!!').notZero(c.get('spin'))

export = m

